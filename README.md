# Veramo DIDComm

Endpoints and flow

POST /users/v1
body: alias
Response: DID Document

- create DID
- create peerID + service info (libp2p)

## Flow

1. Create a new user
2. Create/start a libp2p service
3. Return a DID Document
4. Exchange DID/DID Doc. out-of-band
5. Connect
6. Send messages

POST /users/v1
