import { agent } from "./veramo/setup";
import {
  didDocEndpoint,
  MessagingRouter,
  RequestWithAgentRouter,
} from "@veramo/remote-server";
import express, { Request, Response } from "express";

var bodyParser = require("body-parser");
// import { Server } from "http";
import http from "http";
import { DIDDocument, IIdentifier, IKey, TKeyType } from "@veramo/core";

const port = 3322;
const didCommPort = 3323;
const libp2pAPIPort = 18080;

async function main() {
  // Start DIDComm router
  let didCommEndpointServer: http.Server;
  const requestWithAgent = RequestWithAgentRouter({ agent });
  await new Promise((resolve) => {
    //setup a server to receive HTTP messages and forward them to this agent to be processed as DIDComm messages
    const app = express();
    // app.use(requestWithAgent)
    app.use(
      "/messaging",
      requestWithAgent,
      MessagingRouter({
        metaData: { type: "DIDComm", value: "integration test" },
      })
    );
    didCommEndpointServer = app.listen(didCommPort, () => {
      console.log(
        `DIDComm endpoint at http://localhost:${didCommPort}/messaging`
      );
      resolve(true);
    });
  });

  // Start API server
  const app = express();
  var jsonParser = bodyParser.json();

  // Create a new user
  app.post("/users/v1", jsonParser, postUsersV1);
  // Return user info (DID Document)
  app.get("/users/v1/:alias", jsonParser, getUserByAlias);
  // Create a new service
  app.post("/users/v1/:alias/connections", jsonParser, postUsersV1Connections);

  http
    .createServer(app)
    .listen(port, () => console.log(`Running at http://localhost:${port}/`));
}

export interface UserAlias {
  alias: string;
}

export interface libp2pService {
  id: string;
  type: string;
  routingKeys: string[];
  serviceEndpoint: string;
  accept: string[];
}

// postUsersV1 create a new user
// Endpoint: /users/v1
const postUsersV1 = async (req: Request, response: Response) => {
  // Obtain user alias
  const alias: UserAlias = req.body;
  let identity: IIdentifier;
  console.log(alias);

  // Check if user exists
  let userExists: boolean = true;
  try {
    identity = await agent.didManagerGetByAlias({ alias: alias.alias });
  } catch (error) {
    userExists = false;
  }
  if (userExists) {
    response.status(400);
    response.send("User exists.");
    return;
  }

  let services: libp2pService[];

  // Get peerID from libp2p
  try {
    services = await getLibp2pServices();
    console.log(services);
  } catch (error) {
    console.log(error);
    response.status(500);
    response.send("Internal error - libp2p service unreachable.");
    return;
  }

  try {
    // Create a new identity
    // identity = await agent.didManagerCreate({
    //   alias: alias.alias,
    //   kms: "local",
    // });
    // console.log(identity);

    // Use dummy identity
    identity = await agent.didManagerImport({
      controllerKeyId: "alice-controller-key",
      did: "did:ethr:rinkeby:0xb1F0E194E0381c86b690bBC3048588c60A9df94B",
      provider: "did:ethr:rinkeby",
      alias: alias.alias,
      keys: [
        {
          privateKeyHex:
            "dc19e4b0271cb5d65a3de3e2a31767799eaee65c6deb7be77fb25761f2bb3e9e",
          kms: "local",
          type: "Secp256k1",
          kid: "alice-controller-key",
        },
      ],
    });

    // TODO: At the moment we cannot publish the relay node info in an efficient way
    // For the purpose of the demo we assume clients connect to the same relay nodes
    for (let service of services) {
      try {
        await agent.didManagerAddService({
          did: identity.did,
          service: {
            id: "did:ethr:rinkeby:0xb1F0E194E0381c86b690bBC3048588c60A9df94B#didcomm-v2-libp2p",
            type: "DIDCommMessaging",
            serviceEndpoint: service.serviceEndpoint,
            description: service.routingKeys.toString(),
          },
        });
      } catch (error) {
        console.log(error);
        response.status(400);
        response.send("Invalid request.");
        return;
      }
    }
    // Return the identity
    response.status(200).json(identity);
  } catch (error) {
    console.log(error);
    response.status(400);
    response.send("Invalid request.");
    return;
  }
};

async function getLibp2pServices(): Promise<libp2pService[]> {
  // For now, consider the data is stored on a static `users.json` file
  let uri = `http://localhost:${libp2pAPIPort}/libp2p/v1/properties/did-service`;
  console.log(uri);
  return (
    fetch(uri)
      // the JSON body is taken from the response
      .then((res) => res.json())
      .then((res) => {
        return res as libp2pService[];
      })
  );
}

// Get DID Document by alias
// endpoint: /users/v1/{alias}
const getUserByAlias = async (req: Request, response: Response) => {
  try {
    // Obtain user alias
    const _alias: string = req.params.alias;
    console.log(_alias);
    const identity = await agent.didManagerGetByAlias({ alias: _alias });
    // Return the identity
    const didDoc = await agent.resolveDid({ didUrl: identity.did });
    response.status(200).json(didDoc);
  } catch (error) {
    response.status(400);
    response.send("Invalid request.");
  }
};

export interface did {
  did: string;
}

export interface libp2pConnectionRequest {
  Protocol: string; // Protocol name (in terms of libp2p); e.g., /x/didcomm/v2
  ListenAddress: string; // Local address to which we send the messages
  TargetAddress: string; // Remote libp2p address to which we forward the messages
}

// Start a local listener and try to connect
// Endpoint: /users/v1/:alias/connections
// Header: "Content-Type: application/json"
// Body:
//   did: user did
const postUsersV1Connections = async (req: Request, response: Response) => {
  // Resolve the DID we're trying to connect with
  try {
    const _did: did = req.body;
    const didDocRes = await agent.resolveDid({ didUrl: _did.did });
    let didDoc: DIDDocument;
    if (didDocRes.didDocument) {
      didDoc = didDocRes.didDocument;
    } else {
      response.status(400);
      response.send("Invalid request.");
      return;
    }

    // Extract the libp2p service endpoint
    let serviceEndpoint: string;
    if (didDoc.service?.[0].serviceEndpoint) {
      serviceEndpoint = didDoc.service?.[0].serviceEndpoint;
    } else {
      response.status(400);
      response.send("Invalid request.");
      return;
    }

    // Create a random local port
    const listeningPort = Math.round(Math.random() * 32000 + 2048);

    const lcr: libp2pConnectionRequest = {
      Protocol: "/x/didcomm/v2",
      ListenAddress: `/ip4/127.0.0.1/tcp/${listeningPort}`,
      TargetAddress: serviceEndpoint,
    };

    // establish a libp2p connection

    // I'm not sure what is supposed to happen with these keys in the code below.
    // To pack a DIDComm message, veramo does not need to import the recipent DID or keys, It will resolve them as needed.
    //
    // let keys: IKey[] = [];
    //
    // if (didDoc.verificationMethod) {
    //   for (const i of didDoc.verificationMethod.keys()) {
    //     let vm = didDoc.verificationMethod[i];
    //     if (!vm.type || ! vm.publicKeyHex) {
    //       continue
    //     }
    //     let key: IKey = {
    //       kid: vm.id,
    //       kms: "local",
    //       type: <TKeyType>'Secp256k1',
    //       publicKeyHex: vm.publicKeyHex,
    //     };
    //     keys.push(key);
    //   }
    // }
    //
    // if (didDoc?.verificationMethod) {
    //   agent.didManagerImport({
    //     did: didDoc?.id,
    //     services: didDoc?.service,
    //     provider: "did:ethr:rinkeby",
    //     alias: didDoc.id,
    //     keys: didDoc?.verificationMethod.keys || [],
    //   });
    // }
  } catch (error) {
    response.status(400);
    response.send("Invalid request.");
    return;
  }
};

export interface NewMessage {
  type: string;
  from: string;
  to: string;
  id: string;
  body: object;
}

// Send message
const postNewMessage = async (request: Request, response: Response) => {
  try {
    // Parse a message
    const _message: NewMessage = request.body;
    // Pack a message
    const packedMessage = await agent.packDIDCommMessage({
      packing: "jws",
      message: _message,
    });
    console.log("DIDComm Message:", packedMessage);
    const identity = await agent.didManagerGetByAlias({ alias: _message.to });
    const result = await agent.sendDIDCommMessage({
      messageId: "test-jws-success",
      packedMessage,
      recipientDidUrl: identity.did,
    });
    response.status(200).json(result);
  } catch (error) {
    response.status(400);
    response.send("Invalid request.");
  }
};

// endpoint: connect
// endpoint: listen (open a new listener)
// endpoint: send message(s) plain
// endpoint: send message(s) encrypted

main().catch(console.log);
