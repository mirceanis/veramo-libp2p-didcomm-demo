# LibP2P client

## About The Project

Libp2p client enables to connect with other clients via a P2P overlay network.

## Build with

> go build

## Getting Started

- Client architecture is available TBD
- Documentation is available TBD
- [Functional specifications](docs/functional-specifications.md)

### Prerequisites

[GO](https://golang.org/doc/install) version 1.15 or higher

### Installation

(comming soon)

## Usage

To start a client with default values, run
> ./libp2p-client

For help, run
> ./libp2p-client -h

## Examples

Important note: since communication is relayed via IPFS communication channels, peers are discoverable via: /ipfs/<peer-id> and not via /p2p/<peer-id>

TODO: Check if we can support the /p2p/ protocol (as in IPFS swarm)

### Chat using Netcat

In this section, we demonstrate how to chat over a libp2p overlay network using Netcat.

#### Install Netcat

To install Netcat, see [How to install Netcat](https://linoxide.com/simple-chat-netcat-linux/)

Go to demo/netcat/alice

run: ./start-client.sh

run: ./netcat-chat-over-libp2p.sh

Go to demo/netcat/bob

run: ./start-client.sh

run: ./netcat-chat-over-libp2p.sh

### SSH tunneling example

### Veramo Agent - A hello world example via DIDComm

## Relayed communication

- Auto relay is not (yet) configured correctly
- Relayed communication can be via peers directly (/p2p/<relay-node-id>/p2p-circuit/ipfs/<peerId>)

Hence the DID Document should contain:

- PeerID
- Network
- Protocol
- supported relay nodes

## Roadmap

- demo
- pre-prod
- production

## License

Distributed under the MIT License. See LICENSE for more information.

## Contact

Alen Horvat - alen.horvat@aceblock.com

## References

- [Libp2p multiaddress/enode/enr mapping](https://consensys.net/diligence/blog/2020/09/libp2p-multiaddr-enode-enr/)
- [Veramo DIDComm](https://github.com/uport-project/veramo/tree/next/packages/did-comm)
- [gRPC ALTS](https://grpc.io/docs/languages/go/alts/)
- [Double Ratchet](https://pkg.go.dev/github.com/status-im/doubleratchet)
- [GO - security](https://medium.com/asecuritysite-when-bob-met-alice/me-and-golang-for-cybersecurity-the-gopher-is-nearly-perfect-83c6d1980cc9)
- [Go Kit examples](https://github.com/go-kit)
- [PFS in GO - DHE, ECDHE](https://blog.bracebin.com/achieving-perfect-ssl-labs-score-with-go)
- [Circuit Relay](https://docs.libp2p.io/concepts/circuit-relay/)
- [Libp2p circuit v2](https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md)