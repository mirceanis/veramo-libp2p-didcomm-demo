package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/hyperledger/aries-framework-go/pkg/doc/did"
	"github.com/sirupsen/logrus"

	"github.com/go-kit/kit/endpoint"
	ma "github.com/multiformats/go-multiaddr"
)

// LibP2P node interfaces
type LibP2PNode interface {
	ListenTo(string, string) error
	ConnectTo(string, string, string, bool) error
	ListActiveListeners() ([]string, error)
	CloseListeners([]interface{}) error
	CloseStreams(bool, string) error
}

// ListenToRequest
// Libp2p listener arguments.
// <Protocol> specifies the libp2p handler name
// <TargetAddress> specifies the address to which connections made to the <Protocol> are forwarded to
//
// Example:
// Protocol: /x/test/v1
// TargetAddress: /ip/127.0.0.1/tcp/3000
// Forwards the libp2p connections to 127.0.0.1:3000
type ListenToRequest struct {
	// Libp2p protocol
	Protocol string `json:"Protocol"`
	// Local address
	TargetAddress string `json:"TargetAddress"`
}

// ListenToResponse
// Returns error message
type ListenToResponse struct {
	Err string `json:"err,omitempty"`
}

// ListenToEndpoint
// JSON-RPC endpoint that exposes the libp2p client ListenTo method
func ListenToEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		// Parse the request
		req := request.(ListenToRequest)
		// Create a new listener
		err := n.ListenTo(req.Protocol, req.TargetAddress)
		if err != nil {
			return ListenToResponse{err.Error()}, nil
		}
		return ListenToResponse{""}, nil
	}
}

// decodeListenToRequest
// Decodes the http request
func decodeListenToRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request ListenToRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

// ListActiveListenersRequest
// No input arguments
type ListActiveListenersRequest struct{}

// ListActiveListenersResponse
// Return a list of active libp2p listeners
type ListActiveListenersResponse struct {
	ActiveListeners []P2PListenerInfoOutput `json:"activeListeners,omitempty"`
	Err             string                  `json:"err,omitempty"`
}

// ListActiveListenersEndpoint
// Method that lists active libp2p listeners
func ListActiveListenersEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		// Get a list of active listeners
		activeListeners, err := n.listActiveListeners()
		if err != nil {
			return ListActiveListenersResponse{[]P2PListenerInfoOutput{}, err.Error()}, nil
		}
		return ListActiveListenersResponse{activeListeners.Listeners, ""}, nil
	}
}

// decodeListActiveListenersRequest
func decodeListActiveListenersRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return ListActiveListenersRequest{}, nil
}

type CloseListenersRequest struct{}
type CloseListenersResponse struct{}

// ConnectToRequest
// Arguments to create a libp2p connection
// <Protocol> specifies the libp2p handler name
// <ListenAddress> specifies the address from which the connection is forwarded
// <TargetAddress> specifies the address to which the connection is forwarded
// <AllowCustom>
// <DID> specifies the DID of the client we want to connect with
// <DIDDocument> DID Document of the client we want to conenct with
// Only one of: TargetAddress, DID, DIDDocument MUST be specified
type ConnectToRequest struct {
	Protocol           *string `json:"Protocol,omitempty"`
	ListenAddress      string
	TargetAddress      *string `json:"TargetAddress,omitempty"`
	AllowCustom        bool
	DID                *string      `json:"DID,omitempty"`
	DIDDocument        *did.Doc     `json:"DIDDocument,omitempty"`
	DIDDocumentService *did.Service `json:"DIDDocumentService,omitempty"`
}

// decodeListenToRequest
// Decodes the http request
func decodeConnectToRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request ConnectToRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

// ConnectToResponse
// Returns error message
type ConnectToResponse struct {
	Err string `json:"err,omitempty"`
}

// ConnectToEndpoint
// JSON-RPC endpoint that handles the new libp2p connections
func ConnectToEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(ConnectToRequest)
		// Extract the target address
		protocol, serviceEndpoint, routingKeys, err := getTargetConnectionInfo(req)
		if err != nil {
			return ConnectToResponse{err.Error()}, nil
		}
		// Try to establish a direct connection with the target address
		targetAddress := serviceEndpoint
		// serviceEndpoint must be a multiaddress
		tma, err := ma.NewMultiaddr(targetAddress)
		if err != nil {
			return ConnectToResponse{err.Error()}, nil
		}
		peerId, err := tma.ValueForProtocol(ma.P_IPFS)
		if err != nil {
			return ConnectToResponse{err.Error()}, nil
		}
		err = n.ConnectTo(protocol, req.ListenAddress, targetAddress, req.AllowCustom)
		if err != nil {
			// Peer not available, let's try via relay nodes
			// We loop over all relay nodes and try to connect
			for _, relayNodeId := range routingKeys {
				// Try to connect via relay node
				targetAddress := fmt.Sprintf("/p2p/%s/p2p-circuit/ipfs/%s", relayNodeId, peerId)
				err = n.ConnectTo(protocol, req.ListenAddress, targetAddress, req.AllowCustom)
				if err == nil {
					// We are connected, exit
					logrus.Debugln("[SUCCESS] Connected via:", targetAddress)
					return ConnectToResponse{""}, nil
				}
			}
			return ConnectToResponse{err.Error()}, nil
		}
		logrus.Debugln("[SUCCESS]] Connected via:", targetAddress)

		return ConnectToResponse{""}, nil
	}
}

// extract the target address
// TODO: Error Handling
func getTargetConnectionInfo(req ConnectToRequest) (string, string, []string, error) {
	var serviceEndpoint, protocol string
	var routingKeys []string
	if req.DIDDocumentService != nil {
		// 0. Check if DID Document is set
		protocol = req.DIDDocumentService.Accept[0]
		serviceEndpoint = req.DIDDocumentService.ServiceEndpoint
		routingKeys = req.DIDDocumentService.RoutingKeys
		// service, _ := did.LookupService(req.DIDDocument, "DIDCommMessaging")
		// if service != nil {
		// 	targetAddress = strings.TrimLeft(service.ServiceEndpoint, "libp2p:")
		// 	protocol = "/x/didcomm/v2"
		// }
	} else if req.DIDDocument != nil {
		// 1. Check if DID Document is set
		// service, _ := did.LookupService(req.DIDDocument, "DIDCommMessaging")
		// if service != nil {
		// 	targetAddress = strings.TrimLeft(service.ServiceEndpoint, "libp2p:")
		// 	protocol = "/x/didcomm/v2"
		// }
		return "", "", []string{}, errors.New("[ERROR] Not supported, yet)")
	} else if req.DID != nil {
		// 2. Check if DID is set
		return "", "", []string{}, errors.New("[ERROR] DID Document resolution is not supported, yet)")
	} else if req.TargetAddress != nil {
		// 3. Check if TargetAddress is set
		protocol = *req.Protocol
		serviceEndpoint = *req.TargetAddress
	} else {
		return "", "", []string{}, errors.New("[ERROR] Missing TargetAddress (DID, DID Document or PeerID)")
	}

	return protocol, serviceEndpoint, routingKeys, nil
}

type CloseStreamsRequest struct{}
type CloseStreamsResponse struct{}

// encodeResponse
// Encode the method response
func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
