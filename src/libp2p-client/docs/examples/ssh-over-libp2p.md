# SSH over libP2P

In this example we demonstrate how to use SSH via libp2p network. For example, we want to access an IoT device behind NAT.

We deploy and start a libp2p-client on both server and client. With server/client we refer to an SSH server/client.

We obtain the following DID Documents from the /properties/v1/did-documents endpoint

| Role   | DID                                                | DID Document |
| :----- | :------------------------------------------------- | :----------- |
| Server | did:x:rwI+hXC2cjZFtIZkKKzywPta5xGvy3EwbdCpWQkym3A= | See below    |
| Client | did:x:5vsM+1eZC9HxkNFBI2LR+IW2HKP2F0OTaSr981Lh8Ik= | See below    |

```json
// Server DID Document
{
  "@context": [
    "https://www.w3.org/ns/did/v1"
  ],
  "id": "did:x:rwI+hXC2cjZFtIZkKKzywPta5xGvy3EwbdCpWQkym3A=",
  "service": [
    {
      "id": "",
      "priority": 0,
      "recipientKeys": [
        "rwI+hXC2cjZFtIZkKKzywPta5xGvy3EwbdCpWQkym3A="
      ],
      "routingKeys": [],
      "serviceEndpoint": "libp2p:/p2p/12D3KooWMbXVzgPzFjgAv57m1tddUMfU5TJHKqrVGcz51U9ahEGf",
      "type": "DIDCommMessaging"
    }
  ]
}
// Client DID Document
{
  "@context": [
    "https://www.w3.org/ns/did/v1"
  ],
  "id": "did:x:5vsM+1eZC9HxkNFBI2LR+IW2HKP2F0OTaSr981Lh8Ik=",
  "service": [
    {
      "id": "",
      "priority": 0,
      "recipientKeys": [
        "5vsM+1eZC9HxkNFBI2LR+IW2HKP2F0OTaSr981Lh8Ik="
      ],
      "routingKeys": [],
      "serviceEndpoint": "libp2p:/p2p/12D3KooWRN1yCiS62WxXAuFXeZWsQeozxwKH6iaeBC9ahjWJHzdn",
      "type": "DIDCommMessaging"
    }
  ]
}
```

On the server, we start a listener that listens for incomming connections on the libp2p network and forwards them to the local SSH daemon, that is listening on port 22:

```bash
data='{"Protocol":"/x/ssh/demo","TargetAddress":"/ip4/127.0.0.1/tcp/22"}'
curl -X POST -d ${data} 127.0.0.1:${port}${endpoint}
```

On the client, we connect with the server via libp2p network

```bash
libP2PClientPort=8899
data='{"Protocol":"/x/ssh/demo","ListenAddress":"/ip4/127.0.0.1/tcp/4444","DIDDocument": '${ClientDIDDOcument}''

curl -X POST -d "${data}" 127.0.0.1:${libP2PClientPort}/libp2p/v1/connections
```

Now we can connect to the server using SSH as

```bash
ssh -p 4444 username@localhost
```